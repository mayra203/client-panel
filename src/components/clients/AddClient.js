import React, { Component } from "react";
import { Link } from "react-router-dom";
import { firestoreConnect } from "react-redux-firebase";
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';

class AddClient extends Component {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        balance: ''
    }
    onChange=(e) => this.setState({[e.target.name] : e.target.value})
    onSubmit=(e)=>{
        e.preventDefault()
        const newClient = this.state
        const {firestore, history} = this.props

        if(newClient.balance===""){
            newClient.balance=0
        }

        firestore
            .add({collection: 'clients'}, newClient)
            .then(()=> history.push('/'))

    }

    render() {
        const {disabledBalanceOnAdd} = this.props.settings
        const { firstName, lastName, email, phone, balance } = this.state
        return (
            <div>
                <div className="row">
                    <div className="col-md-6">
                        <Link to="/" className="btn btn-link">
                            <i className="fa fa-arrow-circle-left" aria-hidden="true" />{" "}
                            Back to Dashboard
                        </Link>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header">
                        <h5>Add Client</h5>
                    </div>
                    <div className="card-body">
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label htmlFor="firstName">First Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="firstName"
                                    id="firstName"
                                    placeholder=""
                                    minLength="2"
                                    required
                                    onChange={this.onChange}
                                    value={firstName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="lastName">Last Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    id="lastName"
                                    placeholder=""
                                    minLength="2"
                                    required
                                    onChange={this.onChange}
                                    value={lastName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    name="email"
                                    id="email"
                                    placeholder=""
                                    required
                                    onChange={this.onChange}
                                    value={email}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">Phone</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="phone"
                                    id="phone"
                                    placeholder=""
                                    minLength="10"
                                    required
                                    onChange={this.onChange}
                                    value={phone}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="balance">Balance</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="balance"
                                    id="balance"
                                    placeholder=""
                                    minLength="1"
                                    required
                                    onChange={this.onChange}
                                    value={balance}
                                    disabled= {disabledBalanceOnAdd}
                                />
                            </div>
                            
                            <button type="submit" className="btn btn-primary btn-block">Submit</button>                            
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

AddClient.propTypes = {
    firestore: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
}

export default compose(
    firestoreConnect(),
    connect((state, props)=>({
        settings: state.settings
    }))
)(AddClient);

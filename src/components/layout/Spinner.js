import React from "react";
import "./spinner.css";

export default () => {
    return (
        <div className="text-center">
            <div className="lds-heart">
                <div />
            </div>
        </div>
    );
};

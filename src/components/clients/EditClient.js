import React, { Component } from "react";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import PropTypes from 'prop-types';
import Spinner from "../layout/Spinner";

class EditClient extends Component {
    constructor(props){
        super(props)
        //create refs
        this.firstNameInput= React.createRef()
        this.lastNameInput= React.createRef()
        this.emailInput= React.createRef()
        this.phoneInput= React.createRef()
        this.balanceInput= React.createRef()
    }
    onSubmit = (e) => {
        e.preventDefault()
        const { client, firestore, history } = this.props
        
        const updateClient = {
            firstName: this.firstNameInput.current.value,
            lastName: this.lastNameInput.current.value,
            email: this.emailInput.current.value,
            phone: this.phoneInput.current.value,
            balance: this.balanceInput.current.value==="" ? 0 : this.balanceInput.current.value,
        }

        firestore
            .update({ collection: 'clients', doc: client.id }, updateClient)
            .then(() => history.push('/'))

    }
    render() {

        const {client} = this.props
        const {disabledBalanceOnEdit} = this.props.settings
        if(client){
            return (
                <div>
                    <div className="row">
                        <div className="col-md-6">
                            <Link to="/" className="btn btn-link">
                                <i className="fa fa-arrow-circle-left" aria-hidden="true" />{" "}
                                Back to Dashboard
                            </Link>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h5>Edit Client</h5>
                        </div>
                        <div className="card-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="firstName">First Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="firstName"
                                        id="firstName"
                                        placeholder=""
                                        minLength="2"
                                        required
                                        ref={this.firstNameInput}
                                        defaultValue={client.firstName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="lastName"
                                        id="lastName"
                                        placeholder=""
                                        minLength="2"
                                        required

                                        ref={this.lastNameInput}
                                        defaultValue={client.lastName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        id="email"
                                        placeholder=""
                                        required
                                        ref={this.emailInput}
                                        defaultValue={client.email}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone">Phone</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="phone"
                                        id="phone"
                                        placeholder=""
                                        minLength="10"
                                        required
                                        ref={this.phoneInput}
                                        defaultValue={client.phone}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="balance">Balance</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="balance"
                                        id="balance"
                                        placeholder=""
                                        minLength="1"
                                        required
                                        ref={this.balanceInput}
                                        defaultValue={client.balance}
                                        disabled={disabledBalanceOnEdit}
                                    />
                                </div>

                                <button type="submit" className="btn btn-primary btn-block">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }else{
            return <Spinner/>
        }
    }
}

EditClient.propTypes = {
    firestore: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired
}

export default compose(
    firestoreConnect(props => [
        {
            collection: "clients",
            storeAs: "client",
            doc: props.match.params.id
        }
    ]),
    connect(({firestore: {ordered},settings},props) => ({
        client: ordered.client && ordered.client[0],
        settings: settings
    })))(EditClient);

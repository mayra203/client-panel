import { createStore, combineReducers, compose } from 'redux';
import firebase from 'firebase'
import 'firebase/firestore'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import { reduxFirestore, firestoreReducer }  from 'redux-firestore'
import notifyReducer from './reducers/notifyReducer'
import settingsReducer from './reducers/settingsReducer'

const firebaseConfig = {
    apiKey: "AIzaSyCBJnjyVLPYY-fprC5a9-q4Qmt6t6sYvVw",
    authDomain: "reactclientpanel-mr.firebaseapp.com",
    databaseURL: "https://reactclientpanel-mr.firebaseio.com",
    projectId: "reactclientpanel-mr",
    storageBucket: "reactclientpanel-mr.appspot.com",
    messagingSenderId: "21073956163",
    appId: "1:21073956163:web:d82d83a50665d7b5"
}

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}
firebase.initializeApp(firebaseConfig)

//Init Firestore
firebase.firestore()

// Add reactReduxFirebase enhancer when making store creator
const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig), // firebase instance as first argument
    reduxFirestore(firebase) 
)(createStore)

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    notify: notifyReducer,
    settings: settingsReducer
})
if(localStorage.getItem('settings')==null){
    const defaultSettings = {
        disabledBalanceOnAdd: true,
        disabledBalanceOnEdit: false,
        allowRegistration: false
    }
    localStorage.setItem('settings',JSON.stringify(defaultSettings))
}
//Create Initial state
const initialState = {settings : JSON.parse(localStorage.getItem('settings'))}
//Create store
const store = createStoreWithFirebase(
    rootReducer, 
    initialState, 
    compose(
        reactReduxFirebase(firebase),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
)

export default store